#ifndef PROJECT_INCLUDE_LIST_OF_STRING_H_
#define PROJECT_INCLUDE_LIST_OF_STRING_H_

#include <list>
#include <string>

class List_of_string {
 public:
    List_of_string(std::string const& str, char delimiter);
    void insert(size_t pos, std::string const& new_str);
    void remove(size_t pos);
    void change(size_t pos, std::string const& new_str);
    void print() const noexcept;
    std::string get_string() const noexcept;
 private:
    std::list<std::string> _data;
    char _delimiter;
};


#endif  // PROJECT_INCLUDE_LIST_OF_STRING_H_
