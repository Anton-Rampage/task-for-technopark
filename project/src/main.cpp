#include <iostream>
#include <List_of_string.h>
#include <vector>

int main() {
    std::cout << "Start" << std::endl;
    std::cout << "write new line" << std::endl;
    std::string line;
    std::getline(std::cin, line);
    List_of_string list(line, '\t');
    std::string task;

    while (true) {
        std::cout <<
            "COMMANDS:" << std::endl <<
            "1. remove" << std::endl <<
            "2. insert" << std::endl <<
            "3. change" << std::endl <<
            "4. print current" << std::endl <<
            "5. print on lines" << std::endl <<
            "6. exit" << std::endl;
        std::getline(std::cin >> std::ws, task);
        if (task == "remove") {
            std::cout << "write position" << std::endl;
            size_t pos = 0;
            std::cin >> pos;
            list.remove(pos);
        } else if (task == "insert") {
            std::cout << "write position" << std::endl;
            size_t pos = 0;
            std::cin >> pos;
            std::cout << "write new string" << std::endl;
            std::string str;
            std::getline(std::cin >> std::ws, str);
            list.insert(pos, str);
        } else if (task == "change") {
            std::cout << "write position" << std::endl;
            size_t pos = 0;
            std::cin >> pos;
            std::cout << "write new string" << std::endl;
            std::string str;
            std::getline(std::cin >> std::ws, str);
            list.change(pos, str);
        } else if (task == "print current") {
            std::cout << list.get_string() << std::endl;
        } else if (task == "print on lines") {
            list.print();
        } else if (task == "exit") {
            std::cout << "Bye:)" << std::endl;
            break;
        } else {
            std::cout << "wrong command" <<std::endl;
        }
    }
    return 0;
}
