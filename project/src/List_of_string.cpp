#include "List_of_string.h"
#include <iostream>

List_of_string::List_of_string(std::string const& str, char delimiter) : _delimiter(delimiter) {
    size_t first = 0;

    for (; first <= str.size();) {
        size_t last = str.find(_delimiter, first);

        if (last == std::string::npos) {
            _data.push_back(str.substr(first, str.size() - first + 1));
            break;
        }
        std::string tmp = str.substr(first, last - first);
        _data.push_back(tmp);
        first = ++last;
    }
}

void List_of_string::insert(size_t pos, std::string const& new_str) {
    auto position = _data.begin();
    std::advance(position, pos);
    _data.insert(position, new_str);
}

void List_of_string::remove(size_t pos) {
    auto position = _data.begin();
    std::advance(position, pos);
    _data.erase(position);
}

void List_of_string::change(size_t pos, std::string const& new_str) {
    auto position = _data.begin();
    std::advance(position, pos);
    *position = new_str;
}

std::string List_of_string::get_string() const noexcept {
    std::string res;
    res += *_data.begin();
    for (auto it = ++_data.cbegin(); it != _data.cend(); ++it) {
        res += _delimiter;
        res += *it;
    }
    return res;
}

void List_of_string::print() const noexcept {
    for (const auto& it : _data) {
        std::cout << it << std::endl;
    }
}
