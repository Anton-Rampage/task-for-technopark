#include <gtest/gtest.h>
#include <List_of_string.h>


TEST(test_first_line, test_of_remove) {
    std::string str("Python\tC\tC++\tJava\tGolang");
    List_of_string strings(str, '\t');
    strings.remove(1);
    EXPECT_STREQ(strings.get_string().c_str(), "Python\tC++\tJava\tGolang");
}

TEST(test_first_line, test_of_insert) {
    std::string str("Python\tC\tC++\tJava\tGolang");
    List_of_string strings(str, '\t');
    std::string new_str("Pascal");
    strings.insert(2, new_str);
    EXPECT_STREQ(strings.get_string().c_str(), "Python\tC\tPascal\tC++\tJava\tGolang");
}

TEST(test_first_line, test_of_change) {
    std::string str("Python\tC\tC++\tJava\tGolang");
    List_of_string strings(str, '\t');
    std::string new_str("Pascal");
    strings.change(0, new_str);
    EXPECT_STREQ(strings.get_string().c_str(), "Pascal\tC\tC++\tJava\tGolang");
}

TEST(test_second_line, test_of_remove) {
    std::string str("JoJo Bizzare Adventure\tKonosuba\tRe:Zero\tHigh School DxD\tInterspecies Reviewers\tKeijo!!\t"
                    "Prison School");
    List_of_string strings(str, '\t');
    strings.remove(3);
    EXPECT_STREQ(strings.get_string().c_str(), "JoJo Bizzare Adventure\tKonosuba\tRe:Zero\t"
                                               "Interspecies Reviewers\tKeijo!!\tPrison School");
}

TEST(test_second_line, test_of_insert) {
    std::string str("JoJo Bizzare Adventure\tKonosuba\tRe:Zero\tHigh School DxD\tInterspecies Reviewers\tKeijo!!\t"
                    "Prison School");
    List_of_string strings(str, '\t');
    std::string new_str("Kill La Kill");
    strings.insert(5, new_str);
    EXPECT_STREQ(strings.get_string().c_str(), "JoJo Bizzare Adventure\tKonosuba\tRe:Zero\tHigh School DxD\t"
                                               "Interspecies Reviewers\tKill La Kill\tKeijo!!\tPrison School");
}

TEST(test_second_line, test_of_change) {
    std::string str("JoJo Bizzare Adventure\tKonosuba\tRe:Zero\tHigh School DxD\tInterspecies Reviewers\tKeijo!!\t"
                    "Prison School");
    List_of_string strings(str, '\t');
    std::string new_str("Kill La Kill");
    strings.change(6, new_str);
    EXPECT_STREQ(strings.get_string().c_str(), "JoJo Bizzare Adventure\tKonosuba\tRe:Zero\tHigh School DxD\t"
                                               "Interspecies Reviewers\tKeijo!!\tKill La Kill");
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

