cmake_minimum_required(VERSION 3.13)
project(technopark)
set(CMAKE_CXX_STANDARD 17)

include(gtest.cmake)

add_library(list_of_string STATIC project/src/List_of_string.cpp)
include_directories(project/include)

#program
add_executable(program project/src/main.cpp)
target_link_libraries(program list_of_string)

#tests
find_package (Threads)
add_executable(test gtest/test.cpp)
target_link_libraries(test list_of_string)
target_link_libraries(test gtest ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries(test ${GTEST_LIBRARIES})
